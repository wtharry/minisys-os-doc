# GDB使用

- [GDB使用](#gdb%E4%BD%BF%E7%94%A8)
  - [预先准备](#%E9%A2%84%E5%85%88%E5%87%86%E5%A4%87)
  - [GDB操作指令](#gdb%E6%93%8D%E4%BD%9C%E6%8C%87%E4%BB%A4)

## 预先准备

为了能够正常使用GDB调试，做一些前期工具的准备：

1. BusBlaster下载调试板

    需要保证调试板与开发板和PC机的正确连接，如图所示

    ![](img/0_3_1.jpg)

2. 安装驱动程序

    用来安装驱动的程序名称是zadig_2.1.1.exe，不需要额外自行下载，在安装Open-OCD-0.10.0.3-img那个目录下就已经存在。

    插上下载调试板之后，打开该程序，安装相应的BUSBLASTER驱动程序即可。

    ![](img/0_3_2.png)

## GDB操作指令

在正确编译程序，并且通过loadMIPSfpga.bat脚本将程序烧进开发板子上后，会弹出gdb的调试窗口，类似于下图：

![](img/0_3_3.png)

接着就可以在这个窗口敲入一些命令来对程序进行调试。

以MIPSfpga_Lab02_CProgramming实验为例子，下面图片中展示的是对其进行的几种简单常用的调式命令操作与解释。

![](img/0_3_4.png)

![](img/0_3_5.png)

![](img/0_3_6.png)

更多的GDB命令可以参考[GDB用户手册](http://www.gnu.org/software/gdb/documentation/)