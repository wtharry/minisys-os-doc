# makefile与lds

- [makefile与lds](#makefile%E4%B8%8Elds)
  - [makefile](#makefile)
  - [lds](#lds)

## makefile

由于我们是通过其他平台来编译运行在mips平台的代码, 这种编译方式是交叉编译. 我们的交叉编译工具由Imagination Tech提供.

```makefile
CC = mips-mti-elf-gcc
LD = mips-mti-elf-ld
OD = mips-mti-elf-objdump
OC = mips-mti-elf-objcopy
SZ = mips-mti-elf-size
```

CC是编译器, LD是链接器, OD是二进制文件的解析器. 其他不做介绍.

```makefile
CFLAGS  = -EL -g -march=m14kc -msoft-float -O1 -I . -G0
LDFLAGS = -EL -nostartfiles -N -T scse0_3.lds -G0
```

看到`CFLAGS`, 由于我们的平台是little-endian, 所以要有`-EL`选项. `-I .`指定程序要链接的库, 我们指向自己的`inc`文件夹. 其他选项不做介绍.

看到`LDFLAGS`, 重点是`-T scse0_3.lds`选项, 该选项和lds文件相关.

## lds

lds是链接脚本, 针对我们的mips-mti-elf-ld, 我们有几个地方需要结合bootloader一节进行相互查看.

在描述该项目的`scse0_3.lds`中, 我们需要指定`.reset`段在MIPS复位时的入口处`0xbfc00000`, 这正好是我们`boot/boot.S`开始的地方.

`.except_vec3`段被放在了`0x80000000`开始的地方, 这正好是`boot/start.S`中处理中断的部分, 并且对应的四个入口从`0x80000000`开始偏移`0x0`, `0x80`, `0x100`, `0x180`为四个异常入口.

`.bss`段放在了最后面, 紧跟着`end`.

该文件保证了我们编译的代码能够从MIPS复位时启动并运行下去.
