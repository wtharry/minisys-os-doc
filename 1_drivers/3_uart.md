# 串口

- [串口](#%E4%B8%B2%E5%8F%A3)

## 硬件文档

我们使用的是AXI UART 16550 v2.0.

具体参阅 https://www.xilinx.com/support/documentation/ip_documentation/axi_uart16550/v2_0/pg143-axi-uart16550.pdf

## 控制外设

定时器基地址从`0xB0400000`开始, 根据文档来完成所需的寄存器的读写.

- Receiver Buffer Register和Divisor Latch (Most Significant Byte) Register设置工作的波特率.
- Line Control Register设置工作方式.
- Line Status Register获取工作状态.
- Receiver Buffer Register和Transmitter Holding Register用来暂存收发的数据.

其中Line Status Register比较重要, 是串口正常工作的保证

- 第6位指示Transmitter Empty, 用来保证发送数据的时候数据不会因为CPU速度过快而覆写Transmitter Holding Register内容而导致的数据丢失.
- 第0位指示Data Ready, 用来保证数据逐个接收且不丢失.

初始化时我们设置串口工作在115200, 8n1 parity disabled的状态, 并且不使用中断.
